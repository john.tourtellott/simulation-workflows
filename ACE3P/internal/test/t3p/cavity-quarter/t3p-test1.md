# Test conditions from T3P cavity-quarter example

## Setup
Model: pillboxwg.smtk (from .gen)
Category: T3P

## Boundary Conditions
Side set 1 --> Magnetic
Side set 2 --> Magnetic
Side set 3 --> Absorbing
Side set 4 --> Absorbing
Side set 5 --> Exterior
Side set 6 --> Exterior


## Analysis
Linear Solver --> MUMPS
Maximum Time ---> 5e-9
Time Step ------> 2e-12
Global Order ---> 1
Enable Curved Surfaces --> on


## Beam Info
Beam Loading ------> New
Bunch Type --------> Gaussian
RMS Bunch Length --> 0.01
Number of Sigmas --> 5
Symmetry Factor ---> 4
Charge ------------> 1e-12
Starting Point ----> 0, 0, -0.075
Direction ---------> 0, 0, 1
Source Boundary ---> side set 3


## Monitors
Volume Monitor --> New
Name ------------> mymon
Start Time ------> 0e-9
End Time --------> 5e-9
Time Interval ---> 1e-10


WakeField Monitor --> New
Name ---------------> wakefield
Start Contour ------> Enable, -0.075
End Countour -------> Enable,  0.075
Max Distance -------> 1.4


## Save Simulation
t3p-test1.crf
