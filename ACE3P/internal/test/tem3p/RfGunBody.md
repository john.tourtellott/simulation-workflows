# LCLS RF Gun Coupler Cell

TEM3P ThermoElastic example in CW16

Note that this outline does not include the prerequeist
Omega3P simulation.

## Model

Load RfGunBody.ncdf
SaveAs RfGunBody.smtk


## Template

Load ACE3P.crf
Select Module (show by category): TEM3P ThermoElastic


## Step 1: Thermal Solver Specification

Select "TEM3P Thermal Nonlinear" tab


### Boundary Conditions
surface 1 --> Heat Flux (Neumann) --> 0.0
surface 2 --> Heat Flux (Neumann) --> 0.0
surface 3 --> Heat Flux (Neumann) --> 0.0
surface 4 --> Heat Flux (Neumann) --> 0.0
surface 5 --> Robin
  Robin Constant Factor --> 20000
  Robin Constant Value ---> 440000
surface 6 --> RFHeating
  NERSC Directory -----------> placeholder
  Mode Number ---------------> 0
  Method --------------------> Target Gradient Scaling
    Electrical Conductivity --> 5.8e7
    Target Gradient ----------> 60e6
    Duty Factor --------------> 0.00036
    Start Point --------------> 0.0001, 0.0001 -0.017018
    End Point ----------------> 0.0001, 0.0001 0.017018


### Materials
Material ----------------> New, volume 1
  Material --------------> Custom Constant...
  Thermal Conductivity --> 391


### Analysis
Basis Order ---------> 2
Curved Surfaces -----> On

Linear Solver Type --> CG
Preconditioner ------> DIAGONAL
Absolute Tolerance --> 1e-18
Tolerance -----------> 1e-10
Max Iterations ------> 5001


## Step 2: Elastic (Thermostatic) Solver Specification

Select "TEM3P Elastic" tab

### Boundary Conditions
surface 1 --> Mixed --> Normal, Normal, Displacement --> 0,0,0
surface 2 --> Mixed --> Displacement, Normal, Normal  --> 0,0,0
surface 3 --> Mixed --> Normal, Displacement, Normal  --> 0,0,0
surface 4 --> Normal Loading (Neumann) --> 0.0
surface 5 --> Normal Loading (Neumann) --> 0.0
surface 6 --> LF Detunint
  NERSC Directory ---> placeholder
  Mode Number -------> 0
  Omega3P Id --------> 6
  Method ------------> Gradient
    TargetGradient --> 60e6
    Start Point -----> 0.0001, 0.0001 -0.017018
    End Point -------> 0.0001, 0.0001 0.017018


### Materials
Material --> New, volume 1 --> Copper, 293K


### Analysis
Basis Order ---------> 2
Curved Surfaces -----> On

Linear Solver Type --> CG
Preconditioner ------> DIAGONAL
Absolute Tolerance --> 1e-16
Tolerance -----------> 1e-10
Max Iterations ------> 50000


### Mesh Output
Write Deformed Mesh --------------> off
Write Deformed EM Mesh -----------> on
NERSC Directory ------------------> placeholder
Mesh Deform Scale ----------------> 1
Write Stress/Strain Model Files --> on

## Save Simulation
thermo-elastic.crf
