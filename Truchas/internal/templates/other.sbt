<?xml version="1.0" encoding="utf-8" ?>
<SMTK_AttributeResource Version="3">
  <Categories>
    <Cat>Fluid Flow</Cat>
    <Cat>Heat Transfer</Cat>
    <Cat>Induction Heating</Cat>
    <Cat>Solid Mechanics</Cat>
  </Categories>
  <!-- Attribute Definitions-->
  <Definitions>
    <AttDef Type="mesh" Label="Mesh" BaseType="" Version="0" Unique="true">
      <ItemDefinitions>
        <Double Name="coordinate-scale-factor" Label="Coordinate Scale Factor" Version="0" Optional="true" IsEnabledByDefault="false">
          <Categories>
            <Cat>Fluid Flow</Cat>
            <Cat>Heat Transfer</Cat>
            <Cat>Induction Heating</Cat>
            <Cat>Solid Mechanics</Cat>
          </Categories>
          <DefaultValue>1.0</DefaultValue>
        </Double>
        <Int Name="exodus-block-modulus" Label="Exodus Block Modulus" Version="0" AdvanceLevel="1" Optional="true" IsEnabledByDefault="false">
          <Categories>
            <Cat>Fluid Flow</Cat>
            <Cat>Heat Transfer</Cat>
            <Cat>Induction Heating</Cat>
            <Cat>Solid Mechanics</Cat>
          </Categories>
          <DefaultValue>10000</DefaultValue>
          <RangeInfo>
            <Min Inclusive="false">0</Min>
          </RangeInfo>
        </Int>
      </ItemDefinitions>
    </AttDef>
    <AttDef Type="outputs" Label="Outputs" BaseType="" Version="0" Unique="true">
      <ItemDefinitions>
        <Double Name="start-time" Label="Start Time" Version="0">
          <Categories>
            <Cat>Fluid Flow</Cat>
            <Cat>Heat Transfer</Cat>
            <Cat>Induction Heating</Cat>
            <Cat>Solid Mechanics</Cat>
          </Categories>
          <DefaultValue>0.0</DefaultValue>
          <RangeInfo>
            <Min Inclusive="true">0.0</Min>
          </RangeInfo>
        </Double>
        <Double Name="end-time" Label="End Time" Version="0">
          <Categories>
            <Cat>Fluid Flow</Cat>
            <Cat>Heat Transfer</Cat>
            <Cat>Induction Heating</Cat>
            <Cat>Solid Mechanics</Cat>
          </Categories>
          <DefaultValue>1.0</DefaultValue>
          <RangeInfo>
            <Min Inclusive="true">0.0</Min>
          </RangeInfo>
        </Double>
        <Double Name="output-dt" Label="Initial Output Delta-Time" Version="0">
          <Categories>
            <Cat>Fluid Flow</Cat>
            <Cat>Heat Transfer</Cat>
            <Cat>Induction Heating</Cat>
            <Cat>Solid Mechanics</Cat>
          </Categories>
          <DefaultValue>1.0</DefaultValue>
          <RangeInfo>
            <Min Inclusive="true">0.0</Min>
          </RangeInfo>
        </Double>
        <Group Name="output-times" Label="Additional Output Control" Extensible="true" NumberOfRequiredGroups="0">
          <ItemDefinitions>
            <Double Name="time" Label="Output Times" NumberOfRequiredValues="2">
              <Categories>
                <Cat>Fluid Flow</Cat>
                <Cat>Heat Transfer</Cat>
                <Cat>Induction Heating</Cat>
                <Cat>Solid Mechanics</Cat>
              </Categories>
              <ComponentLabels>
                <Label>After time:</Label>
                <Label>Use delta time:</Label>
              </ComponentLabels>
            </Double>
          </ItemDefinitions>
        </Group>
      </ItemDefinitions>
    </AttDef>
    <AttDef Type="physics" Label="Physics" BaseType="" Version="0" Unique="true">
      <ItemDefinitions>
        <Double Name="fluid-body-force" Label="Fluid Body Force" Version="0" Unique="true" NumberOfRequiredValues="3">
          <Categories>
            <Cat>Fluid Flow</Cat>
            <Cat>Heat Transfer</Cat>
            <Cat>Induction Heating</Cat>
            <Cat>Solid Mechanics</Cat>
          </Categories>
          <ComponentLabels>
            <Label>x:</Label>
            <Label>y:</Label>
            <Label>z:</Label>
          </ComponentLabels>
          <DefaultValue>0.0</DefaultValue>
        </Double>
      </ItemDefinitions>
    </AttDef>
    <AttDef Type="physical-constants" Label="Physical Constants" BaseType="" Version="0" Unique="true">
      <ItemDefinitions>
        <Double Name="absolute-zero" Label="Absolute Zero">
          <DefaultValue>0.0</DefaultValue>
          <Categories>
            <Cat>Fluid Flow</Cat>
            <Cat>Heat Transfer</Cat>
            <Cat>Induction Heating</Cat>
            <Cat>Solid Mechanics</Cat>
          </Categories>
        </Double>
        <Double Name="stefan-boltzmann" Label="Stefan-Boltzmann Constant">
          <DefaultValue>5.67e-8</DefaultValue>
          <Categories>
            <Cat>Fluid Flow</Cat>
            <Cat>Heat Transfer</Cat>
            <Cat>Induction Heating</Cat>
            <Cat>Solid Mechanics</Cat>
          </Categories>
        </Double>
      </ItemDefinitions>
    </AttDef>
    <AttDef Type="altmesh" Lable="AltMesh" BaseType="" Version="0" Unique="true">
      <ItemDefinitions>
        <Double Name="coordinate-scale-factor" Label="Coordinate Scale Factor" Version="0" Optional="true" IsEnabledByDefault="false">
          <Categories>
            <Cat>Induction Heating</Cat>
          </Categories>
          <DefaultValue>1.0</DefaultValue>
          <RangeInfo>
            <Min inclusive="false">0.0</Min>
          </RangeInfo>
        </Double>
      </ItemDefinitions>
    </AttDef>
  </Definitions>
</SMTK_AttributeResource>
