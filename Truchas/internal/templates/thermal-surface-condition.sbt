<?xml version="1.0" encoding="utf-8" ?>
<SMTK_AttributeResource Version="3">
  <Categories>
    <Cat>Heat Transfer</Cat>
  </Categories>
  <!-- Attribute definitions for thermal surface conditions-->
  <Definitions>
    <!-- Boundary condition definitions-->
    <AttDef Type="ht.boundary" BaseType="" Abstract="true" Version="0">
      <AssociationsDef Name="ModelAssociations" Version="0" NumberOfRequiredValues="0" Extensible="true">
        <MembershipMask>face</MembershipMask>
      </AssociationsDef>
    </AttDef>
    <AttDef Type="ht.boundary.dirichlet" Label="Dirichlet" BaseType="ht.boundary" Unique="true" RootName="Dirichlet" Version="0">
      <ItemDefinitions>
        <Double Name="temperature" Label="Temperature">
          <Categories>
            <Cat>Heat Transfer</Cat>
          </Categories>
          <DefaultValue>0.0</DefaultValue>
          <ExpressionType>tabular-function</ExpressionType>
        </Double>
      </ItemDefinitions>
    </AttDef>
    <AttDef Type="ht.boundary.flux" Label="Flux" BaseType="ht.boundary" Unique="true" RootName="Flux" Version="0">
      <ItemDefinitions>
        <Double Name="heat-flux" Label="Heat Flux">
          <Categories>
            <Cat>Heat Transfer</Cat>
          </Categories>
          <DefaultValue>0.0</DefaultValue>
          <ExpressionType>tabular-function</ExpressionType>
        </Double>
      </ItemDefinitions>
    </AttDef>
    <AttDef Type="ht.boundary.HTC" Label="HTC" BaseType="ht.boundary" Unique="true" RootName="HTC" Version="0">
      <ItemDefinitions>
        <Double Name="heat-transfer-coefficient" Label="Heat Transfer Coefficient (h)">
          <Categories>
            <Cat>Heat Transfer</Cat>
          </Categories>
          <DefaultValue>0.0</DefaultValue>
          <ExpressionType>tabular-function</ExpressionType>
        </Double>
        <Double Name="reference-temperature" Label="Reference Temperature (T0)">
          <Categories>
            <Cat>Heat Transfer</Cat>
          </Categories>
          <DefaultValue>0.0</DefaultValue>
          <ExpressionType>tabular-function</ExpressionType>
        </Double>
      </ItemDefinitions>
    </AttDef>
    <AttDef Type="ht.boundary.radiation" Label="Radiation" BaseType="ht.boundary" Unique="true" RootName="Radiation" Version="0">
      <ItemDefinitions>
        <Double Name="emissivity" Label="Emissivity (epsilon)">
          <Categories>
            <Cat>Heat Transfer</Cat>
          </Categories>
          <DefaultValue>0.0</DefaultValue>
          <ExpressionType>tabular-function</ExpressionType>
          <RangeInfo>
            <Min Inclusive="true">0.0</Min>
            <Max Inclusive="true">1.0</Max>
          </RangeInfo>
        </Double>
        <Double Name="ambient-temperature" Label="Ambient Temperature (T infinity)">
          <Categories>
            <Cat>Heat Transfer</Cat>
          </Categories>
          <DefaultValue>0.0</DefaultValue>
          <ExpressionType>tabular-function</ExpressionType>
        </Double>
      </ItemDefinitions>
    </AttDef>
    <!-- Interface condition definitions-->
    <AttDef Type="ht.interface" BaseType="" Abstract="true" Version="0">
      <AssociationsDef Name="ModelAssociations" Version="0" NumberOfRequiredValues="0" Extensible="true">
        <MembershipMask>face</MembershipMask>
      </AssociationsDef>
    </AttDef>
    <AttDef Type="ht.interface.HTC" Label="HTC" BaseType="ht.interface" Unique="true" RootName="ifHTC" Version="0">
      <ItemDefinitions>
        <Double Name="heat-transfer-coefficient" Label="Heat Transfer Coefficient (alpha)">
          <Categories>
            <Cat>Heat Transfer</Cat>
          </Categories>
          <DefaultValue>0.0</DefaultValue>
          <ExpressionType>tabular-function</ExpressionType>
        </Double>
      </ItemDefinitions>
    </AttDef>
    <AttDef Type="ht.interface.radiation" Label="Gap Radiation" BaseType="ht.interface" Unique="true" RootName="ifRadiation" Version="0">
      <ItemDefinitions>
        <Double Name="emissivity" Label="Emissivity (epsilon)">
          <Categories>
            <Cat>Heat Transfer</Cat>
          </Categories>
          <DefaultValue>0.0</DefaultValue>
          <ExpressionType>tabular-function</ExpressionType>
          <RangeInfo>
            <Min Inclusive="true">0.0</Min>
            <Max Inclusive="true">1.0</Max>
          </RangeInfo>
        </Double>
      </ItemDefinitions>
    </AttDef>
  </Definitions>
</SMTK_AttributeResource>
