<?xml version="1.0"?>
<SMTK_AttributeSystem Version="2">

  <Categories>
    <Cat>Fluid Flow</Cat>
  </Categories>

  <Definitions>
    <!-- Boundary Conditions-->
    <AttDef Type="boundaryCondition" Label="Boundary Condition" BaseType="" Version="0" Unique="false"> <!-- it seems like this should be unique but doesn't need to be for openfoam -->
      <AssociationsDef Name="ModelAssociations" Version="0" NumberOfRequiredValues="0" Extensible="true">
        <MembershipMask>face|group</MembershipMask>
      </AssociationsDef>
      <ItemDefinitions>
        <String Name="variable" Label="Variable">
          <ChildrenDefinitions>
            <Double Name="pressure-bc-type" Label="Value" />
            <Double Name="velocity-values" Label="Values" NumberOfRequiredValues="3">
              <ComponentLabels>
                <Label>u</Label>
                <Label>v</Label>
                <Label>w</Label>
              </ComponentLabels>
              <DefaultValue>0.0</DefaultValue>
            </Double>
          </ChildrenDefinitions>
          <DiscreteInfo DefaultIndex="0">
            <Structure>
              <Value Enum="Pressure">pressure</Value>
              <Items>
                <Item>pressure-bc-type</Item>
              </Items>
            </Structure>
            <Structure>
              <Value Enum="Velocity">velocity</Value>
              <Items>
                <Item>velocity-values</Item>
              </Items>
            </Structure>
            <Structure>
              <Value Enum="Inlet Outlet">inletOutlet</Value>
              <Items>
                <Item>velocity-values</Item>
              </Items>
            </Structure>
            <Value Enum="Zero Gradient">zeroGradient</Value>
            <Value Enum="Slip">slip</Value>
          </DiscreteInfo>
        </String>
      </ItemDefinitions>
    </AttDef>
  </Definitions>



</SMTK_AttributeSystem>
